package org.apache.nifi.processors.first;


import org.apache.nifi.annotation.behavior.*;
import org.apache.nifi.expression.ExpressionLanguageScope;

import org.apache.nifi.processor.io.InputStreamCallback;

import java.io.IOException;
import java.io.InputStream;

import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.pdfbox.preflight.PreflightDocument;
import org.apache.pdfbox.preflight.ValidationResult;
import org.apache.pdfbox.preflight.exception.SyntaxValidationException;
import org.apache.pdfbox.preflight.parser.PreflightParser;

import org.apache.pdfbox.preflight.utils.ByteArrayDataSource;

@InputRequirement(InputRequirement.Requirement.INPUT_REQUIRED)
@Tags({"PDF", "validation"})
@CapabilityDescription("Validates PDF for incoming flow files")
public class ValidatePDF extends AbstractProcessor {

    public static final PropertyDescriptor PDF_FILE = new PropertyDescriptor.Builder()
            .name("PDF File")
            .description("Validate the PDF file")
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    public static final Relationship VALID = new Relationship.Builder()
            .name("valid")
            .description("FlowFiles that are successfully validated are routed to this relationship")
            .build();
    public static final Relationship INVALID = new Relationship.Builder()
            .name("invalid")
            .description("FlowFiles cannot be parsed according to the PDF/A specification are routed to this relationship")
            .build();
    public static final Relationship ERROR = new Relationship.Builder()
            .name("error")
            .description("FlowFiles that are not validated (cannot open) are routed to this relationship")
            .build();


    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(PDF_FILE);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(VALID);
        relationships.add(INVALID);
        relationships.add(ERROR);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }


    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }

        ValidationResult[] result = {null};
        ValidationResult[] parserError = {null};

        session.read(flowFile, new InputStreamCallback() {

            @Override
            public void process(InputStream in) throws IOException {

               try {
                    final PreflightParser parser = new PreflightParser(new ByteArrayDataSource(in));
                    parser.parse();
                    PreflightDocument document = parser.getPreflightDocument();
                    document.validate();

                    //PDF file can be parsed.
                    // Get validation result
                    result[0] = document.getResult();

                } catch (SyntaxValidationException e)
                {
                    //if the PDF file can't be parsed.
                     parserError[0] = e.getResult();
                }
            }
        });


        if (parserError[0] == null) {

            // display validation result
            if (result[0].isValid()) {
                getLogger().debug("Successfully validated {} against schema; routing to 'valid'", new Object[]{flowFile});
                session.transfer(flowFile, VALID);

            } else if(!result[0].getErrorsList().isEmpty()) {
                for(ValidationResult.ValidationError error : result[0].getErrorsList()) {

                    flowFile = session.putAttribute(flowFile, "validatepdf.invalid.error", error.getDetails());
                    getLogger().info("------- PDF validation failed: {}", new String[]{error.getDetails()});
                }
                session.transfer(flowFile, INVALID);
            }

        } else {
            for(ValidationResult.ValidationError perror : parserError[0].getErrorsList()) {
                flowFile = session.putAttribute(flowFile, "validatepdf.parser.error", perror.getDetails());
                getLogger().info("------- Cannot parser: {}", new String[]{perror.getDetails()});
            }
            session.transfer(flowFile, ERROR);
        }


        }
}
